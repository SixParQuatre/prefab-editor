using System.Collections;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using UnityEditor;
using UnityEngine;

namespace SixParQuatre
{


    /// <summary>
    /// Base class for the prefab inspector. Give access to the root object to child classes
    /// </summary>
    public abstract class PrefabInspector : UnityEditor.Editor
    {
        public struct PropertyAndLabel
        {
            public GUIContent label;
            public SerializedProperty prop;
            public PropertyAndLabel(SerializedProperty _prop, string overridingLabel)
            {
                prop = _prop;
                label = new GUIContent(overridingLabel);
            }
            public PropertyAndLabel(SerializedProperty _prop)
            {
                prop = _prop;
                label = GUIContent.none;
            }

            public void Show()
            {
                if (prop == null)
                {
                    EditorGUILayout.LabelField(label);
                    return;
                }

                if (label == GUIContent.none)
                    EditorGUILayout.PropertyField(prop, true);
                else
                    EditorGUILayout.PropertyField(prop, label, true);
            }
        }

        public List<PropertyAndLabel> RegisteredProperties => registeredProperties;
        public bool TargetsAreValid()
        {
            foreach (Object obj in targets)
            {
                if (obj == null)
                    return false;
            }
            return true;
        }

        protected SerializedObject Target;

        System.Exception lastException = null;
        string errorCaught = null;
        protected void OnEnable()
        {
            if(target == null)
                return;

            if (TargetsAreValid())
                Target = new SerializedObject(targets);
            else
                Target = new SerializedObject(target);

            errorCaught = null;
            lastException = null;
            try
            {
                OnEnable_Specific();
            }
            catch (System.Exception e)
            {
                errorCaught = GenerateErrorMsg(e, "OnEnable");
            }
        }

        protected virtual void OnEnable_Specific()
        {

        }

        private List<PropertyAndLabel> registeredProperties = new List<PropertyAndLabel>();


        /// <summary>
        /// Register Property for the editor, register properties will show up in the same order as registered.
        /// </summary>
        /// <param name="serializedObject"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected SerializedProperty RegisterProperty(SerializedObject serializedObject, string propertyName, string overridingLabel = null)
        {
            if (serializedObject == null)
                return null;

            SerializedProperty prop = serializedObject.FindProperty(propertyName);
            if (prop == null)
            {
                throw new System.Exception("Couldn't find property of name {propertyName}");
            }

            return RegisterProperty(prop, overridingLabel);

        }

        protected SerializedProperty RegisterProperty(SerializedProperty prop, string overridingLabel = null)
        {
            if (overridingLabel == null)
                registeredProperties.Add(new PropertyAndLabel(prop));
            else
                registeredProperties.Add(new PropertyAndLabel(prop, overridingLabel));
            return prop;
        }
        protected void AddHeader(string title)
        {
            registeredProperties.Add(new PropertyAndLabel(null, title));
        }

        protected void UnregisterProperty(SerializedProperty serializedProperty)
        {
            for (int i = 0; i < registeredProperties.Count; ++i)
            {
                if (registeredProperties[i].prop == serializedProperty)
                {
                    registeredProperties.Remove(registeredProperties[i]);
                    break;
                }
            }
        }


        List<SerializedObject> registeredObjects = new List<SerializedObject>();
        protected virtual void OnInspectorGUI_Specific()
        {
            foreach (PropertyAndLabel prop in registeredProperties)
                prop.Show();
        }

        public enum LockButtonAction
        {
            None,
            HideAndLock,
            ShowAndUnlock
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="window"></param>
        /// <returns>returns if the lock button should be shown</returns>
        public virtual LockButtonAction BeforeLockButton(PrefabEditorWindow window) => LockButtonAction.None;

        public virtual void OnPrefabStageClosed() {}

        public override void OnInspectorGUI()
        {

            if (errorCaught != null)
            {
                GUIStyle helpBoxStyle = GUI.skin.GetStyle("HelpBox");
                bool richText = helpBoxStyle.richText;
                helpBoxStyle.richText = true;
                EditorGUILayout.HelpBox(errorCaught, MessageType.Error);
                helpBoxStyle.richText = richText;
                EditorGUI.BeginDisabledGroup(lastException == null);
                if (GUILayout.Button("Dump in Log for Trace"))
                    ExceptionDispatchInfo.Capture(lastException).Throw();
                EditorGUI.EndDisabledGroup();

                return;
            }



            //Update the registered object
            foreach (SerializedObject obj in registeredObjects)
                obj.Update();

            try
            {
                OnInspectorGUI_Specific();
            }
            catch (System.Exception e)
            {
                //Ignore exit exception since they aren't actually Errors
                if (e is ExitGUIException)
                    throw;
                else
                    errorCaught = GenerateErrorMsg(e, "OnInspectorGUI_Specific");
            }

            //Apply modified properties the registered object
            foreach (SerializedObject obj in registeredObjects)
                obj.ApplyModifiedProperties();
        }

        /// <summary>
        /// Returns a list of child GameObject that can be found under the Object[] targets using the given path. Use '.' to go several level deep
        /// e.g. "FirstChild.ChildOfChild.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static SerializedObject FindChild(SerializedObject serObj, string path, bool failIfOneCannotBeFound = true)
        {
            List<GameObject> children = new List<GameObject>();
            string[] names = path.Split('.');
            foreach (Object obj in serObj.targetObjects)
            {
                bool found = true;
                GameObject go = obj as GameObject;
                foreach (string name in names)
                {
                    found = false;
                    foreach (Transform child in go.transform)
                    {
                        if (child.gameObject.name == name)
                        {
                            found = true;
                            go = child.gameObject;
                            break;
                        }
                    }
                    if (!found)
                        break;
                }
                if (found)
                    children.Add(go);
                else if (failIfOneCannotBeFound)
                    return null;

            }
            return new SerializedObject(children.ToArray());
        }

        /// <summary>
        /// Get a SerializedObject encapsulating all the components of a type present on the targetObjects.
        /// This allows to do FindProperty/PropertyField that impact multiselection. 
        /// If register is true; the returned SerializedObject will have  its Update and ApplyModifiedProperties called automatically
        /// </summary>
        /// 
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="register"></typeparam>
        /// <returns></returns>
        protected SerializedObject GetComponents<T>(SerializedObject serObj, bool register = true) where T : Component
        {
            return GetComponents<T>(serObj.targetObjects, register);
        }
        protected SerializedObject GetComponents<T>(Object[] objects, bool register = true) where T : Component
        {
            List<Object> cptObj = new List<Object>();

            foreach (Object obj in objects)
            {
                GameObject go = obj as GameObject;
                if (go == null)
                    continue;

                Component cpt = go.GetComponent<T>();
                if (cpt != null)
                    cptObj.Add(cpt);
            }
            if (cptObj.Count == 0)
                return null;

            SerializedObject serializedObj = new SerializedObject(cptObj.ToArray());
            if (register)
                registeredObjects.Add(serializedObj);
            return serializedObj;
        }

        public static SerializedObject GetComponents(SerializedObject serializedObject, System.Type type)
        {
            List<Object> cptObj = new List<Object>();

            foreach (Object obj in serializedObject.targetObjects)
            {
                GameObject go = obj as GameObject;
                if (go == null)
                    continue;

                Component cpt = go.GetComponent(type);
                if (cpt != null)
                    cptObj.Add(cpt);
            }
            if (cptObj.Count == 0)
                return null;

            return new SerializedObject(cptObj.ToArray());
        }


        protected void UnregisterObject(SerializedObject serializedObj)
        {
            registeredObjects.Remove(serializedObj);
        }

        /// <summary>
        /// Call this to display the name of the properties right under this object. Useful to figure out data for Unity Components
        /// </summary>
        /// <param name="obj"></param>
        protected void PrintPropertiesName(SerializedObject obj)
        {
            SerializedProperty prop = obj.GetIterator();
            prop.NextVisible(true);
            while (prop.NextVisible(false))
                EditorGUILayout.LabelField(prop.name);
        }

        protected void PrintPropertiesName(SerializedProperty root)
        {
            SerializedProperty prop = root.Copy();
            prop.NextVisible(true);
            while (prop.NextVisible(false))
                EditorGUILayout.LabelField(prop.name);
        }

        protected void Header(string header, bool spaceBefore = true)
        {
            bool richText = GUI.skin.label.richText;
            GUI.skin.label.richText = true;
            if (spaceBefore)
                EditorGUILayout.Space();
            EditorGUILayout.LabelField($"<b>{header}</b>", GUI.skin.label);
            GUI.skin.label.richText = richText;
        }


        protected void RegisterObject(SerializedObject obj)
        {
            registeredObjects.Add(obj);
        }

        protected bool AllNull(SerializedObject serializeObject)
        {
            foreach (Object target in targets)
            {
                if (target == null)
                    return true;
            }
            return true;
        }

        string GenerateErrorMsg(System.Exception e, string functionName)
        {
            lastException = e;
            return $"Something went wrong in {functionName}:\n\n<b>{e.Message}</b>\n\nCheck the error in the log and check that the prefab/instance still match your original structure";
        }

    }

}
