﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace SixParQuatre
{
    /// <summary>
    /// Class for the standalone window which display the Custom Editor of a selected prefab, variants or instance
    /// </summary>
    public class PrefabEditorWindow : EditorWindow
    {
        [MenuItem("Window/General/Prefab Editor")]
        public static void ShowWindow()
        {
            PrefabEditorWindow window = EditorWindow.GetWindow(typeof(PrefabEditorWindow)) as PrefabEditorWindow;
            window.titleContent = new GUIContent("Prefab Editor");
        }


        bool isLocked = false;
        public void OnEnable()
        {
            Selection.selectionChanged += OnSelectionChanged;
            OnSelectionChanged_Internal(false);
            PrefabStage.prefabStageOpened += OnPrefabStageOpened;
            PrefabStage.prefabStageClosing += OnPrefabStageClosing;
        }

        public void OnDisable()
        {
            PrefabStage.prefabStageOpened -= OnPrefabStageOpened;
            PrefabStage.prefabStageClosing -= OnPrefabStageClosing;
        }

        public void OnPrefabStageOpened(PrefabStage stage)
        {
            OnSelectionChanged_Internal(true, stage.prefabContentsRoot);
        }

        public void OnPrefabStageClosing(PrefabStage stage)
        {
            editorContainer.OnPrefabStageClosing(stage.prefabContentsRoot);
            isLocked = false;
            OnSelectionChanged_Internal(false);
        }

        void OnSelectionChanged()
        {
            if (isLocked)
                return;

            OnSelectionChanged_Internal(true);
        }

        void OnSelectionChanged_Internal(bool repaint, Object forcedSelection = null)
        {
            List<Object> safeSelected = new List<Object>();
            if(forcedSelection)
            {
                safeSelected.Add(forcedSelection);
            }
            else
            {
                foreach (Object obj in Selection.objects)
                {
                    GameObject go = obj as GameObject;
                    if (go != null)
                    {
                        GameObject nearestPrefab = PrefabUtility.GetNearestPrefabInstanceRoot(go);
                        if(nearestPrefab == null)
                            nearestPrefab = go;
                        safeSelected.Add(nearestPrefab);
                    }
                }
            }

            if (safeSelected.Count == 0)
                editor = null;
            else
                Editor.CreateCachedEditor(safeSelected.ToArray(), typeof(PrefabEditorsContainer), ref editor);

            if (editor != null)
                editorContainer = editor as PrefabEditorsContainer;
            else
                editorContainer = null;

            if (repaint)
                Repaint();

        }

        Vector2 scrollPos = Vector2.zero;
        Editor editor;
        PrefabEditorsContainer editorContainer;

        bool lockButtonVisible = true;
        void OnGUI()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if(isLocked && !editorContainer.HasValidTargets())
            {
                isLocked = false;
                OnSelectionChanged_Internal(false);
            }

            if (editorContainer != null && editorContainer.FoundOneEditor())
            {
                PrefabInspector current = editorContainer.GetCurrentInspector();
                if (current != null)
                {
                    switch(current.BeforeLockButton(this))
                    {
                            case PrefabInspector.LockButtonAction.HideAndLock:
                                isLocked = true;
                                lockButtonVisible = false;
                            break;
                            case PrefabInspector.LockButtonAction.ShowAndUnlock:
                                isLocked = false;
                                lockButtonVisible = true;
                                //Todo add preference to not reselect the object of the editor we finished customizing
                                Selection.activeObject = current.serializedObject.targetObject;
                            break;
                    }
                }
            }

            //Lock button
            if(lockButtonVisible)
            {
                GUIContent contentForButton = EditorGUIUtility.IconContent(isLocked ? "IN LockButton on" : "IN LockButton");
                if (GUILayout.Button(contentForButton))
                {
                    isLocked = !isLocked;
                    if (!isLocked)
                        OnSelectionChanged_Internal(false);
                }
            }
            EditorGUILayout.EndHorizontal();

            //Editor
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            editor?.OnInspectorGUI();
            EditorGUILayout.EndScrollView();
        }
    }
}
