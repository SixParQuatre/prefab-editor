

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using System.Reflection;

using UnityEditor;

using UnityEngine;

using Object = UnityEngine.Object;

#if UNITY_2021_1_OR_NEWER
using UnityEditor.SceneManagement;
#else
using UnityEditor.Experimental.SceneManagement;
#endif

namespace SixParQuatre
{

    [Serializable]
    public class GameObjectAndEditor
    {
        public GameObject prefab;
        public MonoScript script;
        public PrefabEditorUsage usage = PrefabEditorUsage.All;
    }

    [Serializable]
    public class GameObjectAndDDEditor
    {
        public GameObject prefab;
        public DataDrivenEditor editor;
        public PrefabEditorUsage usage = PrefabEditorUsage.All;
    }

    public enum PrefabEditorUsage
    {
        All,
        OnlyInstances,
        OnlyAssociatedPrefab,
        OnlyPrefabAndVariants
    }

    /// <summary>
    /// Scriptable Object used to serialize the pairing of prefab<->editor in the project.
    /// </summary>
    class PrefabEditorSettings : ScriptableObject
    {
        public List<GameObjectAndEditor> Entries = new List<GameObjectAndEditor>();
        public List<GameObjectAndDDEditor> DataDrivenEntries = new List<GameObjectAndDDEditor>();
       
        static PrefabEditorSettings cachedSettings;

        public static PrefabEditorSettings Instance
        {
            get
            {
                return GetOrCreateAsset();
            }
        }
        static PrefabEditorSettings GetOrCreateAsset()
        {
            if(cachedSettings != null)
                return cachedSettings;

            const string folder = "Assets/Resources/Settings";
            const string filename = nameof(PrefabEditorSettings);
            string path = folder + filename +".asset";
            PrefabEditorSettings settings = null;
#if UNITY_EDITOR
            settings = AssetDatabase.LoadAssetAtPath<PrefabEditorSettings>(path);
            if (settings == null)
            {
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                settings = ScriptableObject.CreateInstance<PrefabEditorSettings>();
                AssetDatabase.CreateAsset(settings, path);
                AssetDatabase.SaveAssets();
            }
#else
            settings = Resources.Load<CustomPrefabInspectors>(filename);
#endif
            cachedSettings = settings;            
            return settings;
        }
#if UNITY_EDITOR
        internal static SerializedObject GetOrCreateSerializedSettings()
        {
            return new SerializedObject(Instance);
        }

        //This function plus a whole in Unity exposed functions: it returns the gameobject that you get to
        //when you click on the > button on a prefab instance/variant
        GameObject GetParentPrefabOrVariant(GameObject go)
        {
            string pathToPrefab = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(go);
            GameObject res = PrefabUtility.GetCorrespondingObjectFromSourceAtPath(go, pathToPrefab);
            if (res == go)
                res = PrefabUtility.GetCorrespondingObjectFromSource(go);
            return res;
        }

        /// <summary>
        /// returns if the selected object has an entry in the prefab editor list
        /// </summary>
        /// <param name="selectedObject"></param>
        /// <returns></returns>
        public bool HasEntry(GameObject selectedObject)
        {
            foreach (GameObjectAndEditor entry in Entries)
            {
                if (entry.prefab == selectedObject)
                    return true;
            }
            return false;

        }

        /// <summary>
        /// returns if the selected object has an entry in the prefab editor list
        /// </summary>
        /// <param name="selectedObject"></param>
        /// <returns></returns>
        public bool HasDataDrivenEntry(GameObject selectedObject)
        {
            foreach (GameObjectAndDDEditor entry in DataDrivenEntries)
            {
                if (entry.prefab == selectedObject)
                    return true;
            }
            return false;

        }

        public List<PrefabInspector> FindEditorsFor(Object[] targetObjects)
        {
            //Build the dictionary of prefab inspector -> selectedObject for which this prefab inspector can be used
            Dictionary<MonoScript, List<Object>> pairing = new Dictionary<MonoScript, List<Object>>();
            Dictionary<DataDrivenEditor, List<Object>> pairings2 = new Dictionary<DataDrivenEditor, List<Object>>();
            foreach (Object targetObject in targetObjects)
                FindEditorsFor(targetObject as GameObject, pairing, pairings2);

            //Create all the inspectors from the script
            List<PrefabInspector> prefabEditors = new List<PrefabInspector>();
            foreach (MonoScript sc in pairing.Keys)
            {
                Type t = Type.GetType(sc.GetClass().AssemblyQualifiedName);
                PrefabInspector prefabEditor = Editor.CreateEditor(pairing[sc].ToArray(), t) as PrefabInspector;
                if (prefabEditor != null)
                {
                    prefabEditors.Add(prefabEditor);
                }
            }

            //Create the inspectors from the ddt
            foreach (DataDrivenEditor ddt in pairings2.Keys)
            {
                DataDrivenPrefabInspector prefabEditor = Editor.CreateEditor(pairings2[ddt].ToArray(), typeof(DataDrivenPrefabInspector)) as DataDrivenPrefabInspector;
                if (prefabEditor != null)
                {
                    
                    prefabEditor.SetData(ddt);
                    prefabEditors.Add(prefabEditor);
                }
            }
            
            
            return prefabEditors;
        }
        
        void FindEditorsFor(GameObject gameObject, Dictionary<MonoScript, List<Object>> pairings, Dictionary<DataDrivenEditor, List<Object>> pairings2)
        {
           
            GameObject prefab = gameObject;
            //When staged, a prefab is actually an instance but it will fail to find its prefab so we grab it manually
            PrefabStage prefabStage = PrefabStageUtility.GetPrefabStage(gameObject);
            if(prefabStage != null && prefabStage.prefabContentsRoot == prefab)
                prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabStage.assetPath);

            //We search editor for the object as well as its prefab hierarchy,
            //in case it's an instance of a variant of a variant etc... of a prefab.
            while (prefab != null)
            {
                foreach (GameObjectAndEditor entry in Entries)
                {
                    if (entry.prefab == null)
                        continue;

                    if (entry.prefab != prefab)
                        continue;


                    if (!IsCompatible(gameObject, entry.prefab, entry.usage))
                        continue;

                    MonoScript sc = entry.script;
                    if (sc != null)
                    {
                        if (!pairings.ContainsKey(sc))
                            pairings.Add(sc, new List<Object>());
                        pairings[sc].Add(gameObject);
                    }
                }

                foreach (GameObjectAndDDEditor entry in DataDrivenEntries)
                {
                    if (entry.prefab == null)
                        continue;

                    if (entry.prefab != prefab)
                        continue;


                    if (!IsCompatible(gameObject, entry.prefab, entry.usage))
                        continue;

                    DataDrivenEditor ddt = entry.editor;
                    if (ddt != null)
                    {
                        ddt.canBeCustomized = true; // Todo, this needs to be true ONLY if the gameObject is exactly the one selected in the settings.
                        // But right now; prefab == entry.prefab returns always true
                        if (!pairings2.ContainsKey(ddt))
                            pairings2.Add(ddt, new List<Object>());
                        pairings2[ddt].Add(gameObject);
                    }
                }

                GameObject parentPrefab = GetParentPrefabOrVariant(prefab);
                if (parentPrefab == prefab)
                    break;
                prefab = parentPrefab;
                
            }
        }

        static public bool IsCompatible(GameObject selectedObject, GameObject pairedToScript, PrefabEditorUsage usage)
        {
            if (usage == PrefabEditorUsage.All)
                return true;

            if (usage == PrefabEditorUsage.OnlyAssociatedPrefab && pairedToScript != selectedObject)
                return false;

            bool isInstance = selectedObject.scene.IsValid();
            if (usage == PrefabEditorUsage.OnlyInstances && !isInstance)
                return false;

            //basically anything but an instance
            if (usage == PrefabEditorUsage.OnlyPrefabAndVariants && isInstance)
                return false;

            return true;
        }
#endif
    }

#if UNITY_EDITOR
    // Register a SettingsProvider using IMGUI for the drawing framework:
    static class CustomPrefabInspectorsIMGUIRegister
    {
        [SettingsProvider]
        public static SettingsProvider CreateCustomPrefabInspectorsProvider()
        {
            var ourKeyWords = new HashSet<string>();
            System.Type aType = typeof(PrefabEditorSettings);
            foreach (FieldInfo field in aType.GetFields())
            {
                ourKeyWords.Add(field.Name);
            }
            // First parameter is the path in the Settings window.
            // Second parameter is the scope of this setting: it only appears in the Project Settings window.
            var provider = new SettingsProvider("Project/Six-par-Quatre/Prefab Editors", SettingsScope.Project)
            {
                // By default the last token of the path is used as display name if no label is provided.
                label = "Prefab Editors",
                // Create the SettingsProvider and initialize its drawing (IMGUI) function in place:
                guiHandler = (searchContext) =>
                {
                    var settings = PrefabEditorSettings.GetOrCreateSerializedSettings();
                    var set = settings.targetObject as PrefabEditorSettings;


                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Prefab");
                    EditorGUILayout.LabelField("Associated Editor");
                    EditorGUILayout.LabelField("Show for");
                    EditorGUILayout.LabelField("", GUILayout.Width(20));
                    EditorGUILayout.EndHorizontal();/**/

                    SerializedProperty entries = settings.FindProperty(nameof(PrefabEditorSettings.Entries));
                    if (entries != null)
                    {

                        for (int i = 0; i < entries.arraySize;)
                        {
                            SerializedProperty entryProp = entries.GetArrayElementAtIndex(i);
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.PropertyField(entryProp.FindPropertyRelative(nameof(GameObjectAndEditor.prefab)), GUIContent.none);
                            EditorGUILayout.PropertyField(entryProp.FindPropertyRelative(nameof(GameObjectAndEditor.script)), GUIContent.none);
                            EditorGUILayout.PropertyField(entryProp.FindPropertyRelative(nameof(GameObjectAndEditor.usage)), GUIContent.none);
                            if (GUILayout.Button("x", GUILayout.Width(20)))
                                entries.DeleteArrayElementAtIndex(i);
                            else
                                i++;
                            EditorGUILayout.EndHorizontal();
                        }
                    }

                    SerializedProperty entries2 = settings.FindProperty(nameof(PrefabEditorSettings.DataDrivenEntries));
                    if (entries2 != null)
                    {
                        for (int i = 0; i < entries2.arraySize;)
                        {
                            SerializedProperty entryProp = entries2.GetArrayElementAtIndex(i);
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.PropertyField(entryProp.FindPropertyRelative(nameof(GameObjectAndDDEditor.prefab)), GUIContent.none);
                            EditorGUILayout.PropertyField(entryProp.FindPropertyRelative(nameof(GameObjectAndDDEditor.editor)), GUIContent.none);
                            EditorGUILayout.PropertyField(entryProp.FindPropertyRelative(nameof(GameObjectAndDDEditor.usage)), GUIContent.none);
                            if (GUILayout.Button("x", GUILayout.Width(20)))
                                entries2.DeleteArrayElementAtIndex(i);
                            else
                                i++;
                            EditorGUILayout.EndHorizontal();
                        }
                    }

                    settings.ApplyModifiedProperties();

                },

            
                // Populate the search keywords to enable smart search filtering and label highlighting:
                keywords = ourKeyWords
            };

            return provider;
        }



        const string actionName = "C# Prefab Editor";
        const string menuLocation = "Assets/Create/Prefab Editor/"+ actionName;
        [MenuItem(menuLocation, priority = 81)]
        public static void CreateCustomEditorForPrefab()
        {
            GameObject prefab = Selection.gameObjects[0];
            if (PrefabEditorSettings.Instance.HasEntry(prefab))
            {
                bool ret = EditorUtility.DisplayDialog(actionName, "There is already an entry for this prefab. Do you want to add another entry ? \\n (In doubt, check Project Settings/Custom Prefab Inspectors)", "Yes", "No");
                if(!ret)
                    return;
            }

            //1. Extract the folder from the prefab path
            string path = AssetDatabase.GetAssetPath(prefab);
            int indexSlash = path.LastIndexOf("/");
            string folder = path.Substring(0, indexSlash);
            
            string filename = prefab.name;

            //Classes names don't like spaces, dots or starting with a Digit
            filename = filename.Replace(' ', '_');
            filename = filename.Replace('.', '_');
            if (Char.IsDigit(filename[0]))
                filename = "The" + filename;
            filename += "Editor";

            string scFolder = folder + "/Editor/";
            string scPath = scFolder + filename + ".cs";
            string fullPath = Application.dataPath + scPath.Substring("Assets".Length);

            //2. Find the template asset - we need to do this as we do not know where this 'plugin' folder might be in the project
            string pathForTemplate = "";
            string expectedFileName = "PrefabInspector.cstemplate";
            foreach (string guid in AssetDatabase.FindAssets("PrefabInspector t:DefaultAsset"))
            {
                string resPath = AssetDatabase.GUIDToAssetPath(guid);
                string resFilename = resPath.Substring(resPath.LastIndexOf("/") + 1);
                if (resFilename == expectedFileName)
                {
                    pathForTemplate = resPath;
                    break;
                }
            }
			
			Debug.Log(pathForTemplate);
			string fullpath = Path.GetFullPath(pathForTemplate);
            //3. Read the tempalte and replace the {0} with the prefab name
			
			Debug.Log(fullpath);
            string filledInTemplate = File.ReadAllText(fullpath) ;
            filledInTemplate = string.Format(filledInTemplate, filename);

            //4. We can't create a MonoScript, so we...
            //4a. make sure the director path exists
            Directory.CreateDirectory(Application.dataPath + scFolder.Substring("Assets".Length));
            //4b. create the file with the content
            File.WriteAllText(fullPath, filledInTemplate);
            //4c. Refresh the database so loadAsssetAtPath will not return null.
            AssetDatabase.Refresh();
            MonoScript sc = AssetDatabase.LoadAssetAtPath<MonoScript>(scPath);

            //5. Add the Prefab, MonoScript entry in the settings. 
            SerializedObject settingsObj = PrefabEditorSettings.GetOrCreateSerializedSettings();
            SerializedProperty entries = settingsObj.FindProperty(nameof(PrefabEditorSettings.Entries));
            entries.arraySize++;
            SerializedProperty entry = entries.GetArrayElementAtIndex(entries.arraySize - 1);
            entry.FindPropertyRelative(nameof(GameObjectAndEditor.prefab)).objectReferenceValue = prefab;
            entry.FindPropertyRelative(nameof(GameObjectAndEditor.script)).objectReferenceValue = sc;
            //We use the SerializedObject/Properties so the object properly dirtied.
            settingsObj.ApplyModifiedPropertiesWithoutUndo();

            //Open Asset in default program
            AssetDatabase.OpenAsset(sc);
        }

        [MenuItem(menuLocation, true)]
        public static bool CreateCustomEditorForPrefab_Show()
        {
            return Selection.gameObjects.Length == 1;
        }

       
    }

   
    
#endif
}
