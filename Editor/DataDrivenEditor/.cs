using UnityEditor;

namespace SixParQuatre
{
    [CustomPropertyDrawer(typeof(DataDrivenEditorLineDrawer))]
    public class DataDrivenEditorLineDrawerDrawer : PropertyDrawer
    {
        ppublic override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, true);
		}
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
		
		}
	}
}