using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

namespace SixParQuatre
{
	[Serializable]
	public class DataDrivenEditorLine
	{
		public enum Type
		{
			Property,
			Header
		}

		public Type type = Type.Property;
		//
		public string PropertyPath;
		public string ObjectPath;
		public string TargetComponent;
		[Tooltip("Override Default Display Name")]
		public bool OverrideDisplayName;
		public string OverrideName;
		public string DisplayName;
		//
		public string header = "header";
		public DataDrivenEditorLine(string _header)
		{
			header = _header;
			type = Type.Header;
		}
		public DataDrivenEditorLine(GameObject root, SerializedProperty prop)
		{
			Component cpt = prop.serializedObject.targetObject as Component;
			GameObject propGo = cpt.gameObject;
			bool first = true;
			while (propGo != root)
			{
				Transform pT = propGo.transform.parent;
				if (pT == null)
					break;

				if (!first)
					ObjectPath = "." + ObjectPath;
				else
					first = false;
				ObjectPath = propGo.name + ObjectPath;

				propGo = pT.gameObject;

			}
			DisplayName = prop.displayName;
			PropertyPath = prop.propertyPath;
			TargetComponent = cpt.GetType().AssemblyQualifiedName;
		}


	}
}