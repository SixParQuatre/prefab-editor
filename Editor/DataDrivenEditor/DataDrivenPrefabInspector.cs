using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

namespace SixParQuatre
{

	public class DataDrivenPrefabInspector : PrefabInspector
	{
        List<SerializedProperty> cachedProperties = new List<SerializedProperty>();
        List<string> cachedPropertiesError = new List<string>();
        List<SerializedObject> serializedObjectsToUpdate = new List<SerializedObject>();

        static public DataDrivenEditor Editing = null;
        static public GameObject EditingTarget = null;

        DataDrivenEditor ddt;
        SerializedObject dataDrivenEditor;

        public void SetData(DataDrivenEditor _ddt)
        {
            ddt = _ddt;
            Init();
        }

        Texture renameIconTxt = null;
        void Init()
        {
            renameIconTxt = AssetDatabase.LoadAssetAtPath<Texture>("Packages/com.sixparquatre.prefabeditor/Editor/Resources/RenameIcon.png");
            dataDrivenEditor = new SerializedObject(ddt);

            cachedProperties.Clear();
            serializedObjectsToUpdate.Clear();

            SerializedProperty props = dataDrivenEditor.FindProperty(nameof(DataDrivenEditor.props));
            for (int i = 0; i < props.arraySize; ++i)
            {
                SerializedObject subObject = serializedObject;
                SerializedProperty entry = props.GetArrayElementAtIndex(i);
                SerializedProperty pathField = entry.FindPropertyRelative(nameof(DataDrivenEditorLine.ObjectPath));

                SerializedProperty typeProp = entry.FindPropertyRelative(nameof(DataDrivenEditorLine.type));
                DataDrivenEditorLine.Type sType = (DataDrivenEditorLine.Type)typeProp.enumValueIndex;

                if (sType != DataDrivenEditorLine.Type.Property)
                {
                    cachedProperties.Add(null);
                    cachedPropertiesError.Add(null);
                    continue;
                }

                string objectPath = pathField.stringValue;
                bool onSubObject = !String.IsNullOrEmpty(objectPath);
                if (onSubObject)
                    subObject = PrefabInspector.FindChild(serializedObject, objectPath);

                if(subObject == null)
                {
                    cachedPropertiesError.Add($"Couldn't find subobject of path '{objectPath}' under the root object");
                    cachedProperties.Add(null);
                    continue;       
                }

                string targetComponent = entry.FindPropertyRelative(nameof(DataDrivenEditorLine.TargetComponent)).stringValue;
                Type type = Type.GetType(targetComponent);

                SerializedObject ser = PrefabInspector.GetComponents(subObject, type);
                if(ser == null)
                {
                    if(onSubObject)
                        cachedPropertiesError.Add($"Couldn't find Component '{type.Name}' on subobject of path '{objectPath}' under the root object");
                    else
                        cachedPropertiesError.Add($"Couldn't find Component '{type.Name}' on root object");
                    cachedProperties.Add(null);
                    continue;
                }

                string propertyPath = entry.FindPropertyRelative(nameof(DataDrivenEditorLine.PropertyPath)).stringValue;

                SerializedProperty prop = ser.FindProperty(propertyPath);
                if(prop == null)
                {
                    if(onSubObject)
                        cachedPropertiesError.Add($"Couldn't find property '{propertyPath}' in Component '{type.Name}' in subobject of path '{objectPath}' under the root object");
                    else
                        cachedPropertiesError.Add($"Couldn't find property '{propertyPath}' in Component '{type.Name}' on the root object");
                    cachedProperties.Add(null);
                    continue;
                }

                cachedProperties.Add(prop);
                cachedPropertiesError.Add(null);

                if (!serializedObjectsToUpdate.Contains(ser))
                    serializedObjectsToUpdate.Add(ser);

            }

            if(props.arraySize == 0)
                StartEditing();
        }
        public override void OnPrefabStageClosed() => StopEditing();

        public override LockButtonAction BeforeLockButton(PrefabEditorWindow window)
        {
            if(!ddt.canBeCustomized)
                return LockButtonAction.None;

            LockButtonAction action = LockButtonAction.None;
            if (GUILayout.Button(!Editing ? "Customize Editor" : "Done"))
                action = Editing ? StopEditing() : StartEditing();
            return action;
        }

        LockButtonAction StartEditing()
        {
            Editing = ddt;
            EditingTarget = serializedObject.targetObject as GameObject;
            return LockButtonAction.HideAndLock;
        }

        LockButtonAction StopEditing()
        {
            Init();
            Editing = null;
            EditorUtility.SetDirty(ddt);
            AssetDatabase.SaveAssetIfDirty(ddt);
            return LockButtonAction.ShowAndUnlock;
        }


        public override void OnInspectorGUI()
        {
            bool inEditMode = Editing != null;
            
            foreach (SerializedObject obj in serializedObjectsToUpdate)
            {
                if(obj != null)
                    obj.Update();
            }
            dataDrivenEditor.Update();

            SerializedProperty props = dataDrivenEditor.FindProperty(nameof(DataDrivenEditor.props));
            if (props.arraySize != cachedProperties.Count)
            {
                if (Event.current.type != EventType.Layout)
                    Init();
                return;
            }

            for (int i = 0; i < props.arraySize; ++i)
            {
                SerializedProperty prop = props.GetArrayElementAtIndex(i);
                SerializedProperty typeProp = prop.FindPropertyRelative(nameof(DataDrivenEditorLine.type));
                DataDrivenEditorLine.Type sType = (DataDrivenEditorLine.Type)typeProp.enumValueIndex;

                EditorGUILayout.BeginHorizontal();

                switch (sType)
                {
                    case DataDrivenEditorLine.Type.Header:
                        DisplayHeader(prop, inEditMode);
                        break;

                    case DataDrivenEditorLine.Type.Property:
                        SerializedProperty cachedProp = cachedProperties[i];
                        if (cachedProp == null && string.IsNullOrEmpty(cachedPropertiesError[i]))
                            Init();
                        else
                            DisplayProperty(prop, cachedProp, cachedPropertiesError[i], inEditMode);
                        break;
                }


                //Buttons to change order of properties in layout
                //Optimally, we'd reuse a Reordable array but the + widgt doesn't make sense and as of Deb 2024, cannot be hidden.
                //Plus, the grab widget at the beginning on each line, makes it harder to picture what the final layout looks like
                //with the headers
                bool layoutChanged = false;
                if (inEditMode)
                {
                    GUILayout.FlexibleSpace();
                    int iconWidth = 20;
                    if (i > 0 && GUILayout.Button("↑", GUILayout.Width(iconWidth)))
                    {
                        layoutChanged = true;
                        props.MoveArrayElement(i, i - 1);
                        Swap(cachedProperties, i, i - 1);
                        Swap(cachedPropertiesError, i, i - 1);
                    }
                    if (i < props.arraySize - 1 && GUILayout.Button("↓", GUILayout.Width(iconWidth)))
                    {
                        layoutChanged = true;
                        props.MoveArrayElement(i, i + 1);
                        Swap(cachedProperties, i, i + 1);
                        Swap(cachedPropertiesError, i, i + 1);
                    }
                    if (GUILayout.Button("x", GUILayout.Width(iconWidth)))
                    {
                        layoutChanged = true;
                        props.DeleteArrayElementAtIndex(i);
                        cachedProperties.RemoveAt(i);
                        cachedPropertiesError.RemoveAt(i);
                    }
                }

                EditorGUILayout.EndHorizontal();
                if (layoutChanged)
                    break;
            }

            if (inEditMode)
            {
                if (GUILayout.Button("Add Header"))
                {
                    props.arraySize++;
                    SerializedProperty entry = props.GetArrayElementAtIndex(props.arraySize - 1);
                    SerializedProperty typeProp = entry.FindPropertyRelative(nameof(DataDrivenEditorLine.type));
                    typeProp.enumValueIndex = (int)DataDrivenEditorLine.Type.Header;
                    entry.FindPropertyRelative(nameof(DataDrivenEditorLine.header)).stringValue = "Header";
                }

                string txt = "To add properties, right click on any property of the prefab or its sub-objects and select 'Add to Prefab Editor'\n\nPress 'Done' to see resulting editor.";
                EditorGUILayout.HelpBox(txt, MessageType.Info);

                dataDrivenEditor.ApplyModifiedProperties();
            }
            else
                foreach (SerializedObject obj in serializedObjectsToUpdate)
                {
                    if(obj != null)
                        obj.ApplyModifiedProperties();
                }
        }

        static void Swap<T>(IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }

        void  DisplayHeader(SerializedProperty propEntry, bool inEditMode)
        {
            SerializedProperty hdr = propEntry.FindPropertyRelative(nameof(DataDrivenEditorLine.header));
            if (!inEditMode)
                EditorGUILayout.LabelField(hdr.stringValue, EditorStyles.boldLabel);
            else
                EditorGUILayout.PropertyField(hdr, GUIContent.none, GUILayout.Width(EditorGUIUtility.labelWidth + 20 + EditorGUIUtility.standardVerticalSpacing));
        }

        void DisplayProperty(SerializedProperty propEntry, SerializedProperty cachedProp, string cachedPropError, bool inEditMode)
        {
            SerializedProperty ovrd = propEntry.FindPropertyRelative(nameof(DataDrivenEditorLine.OverrideDisplayName));
            SerializedProperty ovrdVal = propEntry.FindPropertyRelative(nameof(DataDrivenEditorLine.OverrideName));

            if(!string.IsNullOrEmpty(cachedPropError))
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("", GUILayout.Width(20));
                EditorGUILayout.HelpBox(cachedPropError, MessageType.Error);
                EditorGUILayout.EndHorizontal();
                return;
            }
            if (!inEditMode)
            {
                EditorGUILayout.LabelField("", GUILayout.Width(20));
                if (ovrd.boolValue)
                    EditorGUILayout.PropertyField(cachedProp, new GUIContent(ovrdVal.stringValue));
                else
                    EditorGUILayout.PropertyField(cachedProp);
            }
            else
            {
                if(ToggleButton_BigIcon(ovrd, 20 , null, renameIconTxt, 0.8f))
                {
                    if (String.IsNullOrEmpty(ovrdVal.stringValue))
                        ovrdVal.stringValue = cachedProp.displayName;
                    EditorGUILayout.PropertyField(ovrdVal, GUIContent.none, GUILayout.Width(EditorGUIUtility.labelWidth));
                    EditorGUI.BeginDisabledGroup(true);
                    EditorGUILayout.PropertyField(cachedProp, GUIContent.none, false);
                    EditorGUI.EndDisabledGroup();
                }
                else
                {
                    EditorGUI.BeginDisabledGroup(true);
                    EditorGUILayout.PropertyField(cachedProp, false);
                    EditorGUI.EndDisabledGroup();

                }

            }
        }
        
        static float brighteningFactor = 1.7f;
        bool ToggleButton_BigIcon(SerializedProperty prop, float width, string icon, Texture iconTxt = null, float extraIconScale = 1)
        {
            bool val = prop.boolValue;
            ToggleButton_BigIcon(ref val, prop.tooltip, width, icon, iconTxt, extraIconScale);
            prop.boolValue = val;
            return val;
        }
        bool ToggleButton_BigIcon(ref bool state, string tooltip, float width, string icon, Texture iconTxt = null, float extraIconScale = 1)
        {
            bool pressed = false;
            Color savedBackgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = state ? savedBackgroundColor * brighteningFactor : savedBackgroundColor;
         
            if (BigIconButton(icon, iconTxt, tooltip, true, width, -1, extraIconScale))
            {
                state = !state;
                pressed = true;
            }
            GUI.backgroundColor = savedBackgroundColor;
            return pressed;
        }
        /// <summary>
        /// Draws a button with an icon that maximises its size. This is done by doing a blank button and drawing on top:
        /// - if iconTxt isn't null, draw a text
        /// - if iconTxt is null, use icon string in  a label 
        /// </summary>
        /// <returns>returns if the button was pressed</returns>
        public static bool BigIconButton(string icon, Texture iconTxt, string tooltip, bool enabled, float width, float height = -1, float extraIconScale = 1)
        {
            
            EditorGUI.BeginDisabledGroup(!enabled);

            GUIContent button = new GUIContent("", tooltip);
            if (height == -1)
                height = width;
            bool ret = GUILayout.Button(button, GUILayout.Width(width), GUILayout.Height(height));

            Rect rec = GUILayoutUtility.GetLastRect();
            rec.x += rec.width - width;
            rec.height = height * extraIconScale;
            rec.y -= (height * (extraIconScale - 1) * 0.5f);
            rec.width = width * extraIconScale;
            rec.x -= (width * (extraIconScale - 1) * 0.5f);

            if (iconTxt != null)
            {
                GUI.DrawTexture(rec, iconTxt);
            }
            else
            {
                GUIContent copyButtonCt = EditorGUIUtility.IconContent(icon, $"|{tooltip}");
                copyButtonCt.tooltip = tooltip;
                GUI.Label(rec, copyButtonCt);
            }

            EditorGUI.EndDisabledGroup();
            return ret;

        }

    }
}