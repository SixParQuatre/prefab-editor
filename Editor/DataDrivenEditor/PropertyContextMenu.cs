using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

#if UNITY_2021_1_OR_NEWER
using UnityEditor.SceneManagement;
#else
using UnityEditor.Experimental.SceneManagement;
#endif

namespace SixParQuatre
{
	[InitializeOnLoad]
	public static class PropertyContextMenu
	{
		[InitializeOnLoadMethod]
		 static void Init()
		{
			EditorApplication.contextualPropertyMenu += OnContextualPropertyMenu;
		}

		public static DataDrivenEditorLine cachedProp = null;
		private static void OnContextualPropertyMenu(GenericMenu menu, SerializedProperty property)
        {
			cachedProp = null;
			if (DataDrivenPrefabInspector.Editing == null)
				return;
				
			// parent of the serialized object needs to be a Component, not a ScriptableObject
			foreach(UnityEngine.Object obj in property.serializedObject.targetObjects)
			{
				Component cpt = obj as Component;
				if (cpt == null)
					return;
			}

			cachedProp = new DataDrivenEditorLine(DataDrivenPrefabInspector.EditingTarget, property);
			menu.AddItem(new GUIContent("Add to Prefab Editor"), false, () =>
			{
				DataDrivenPrefabInspector.Editing.Add(cachedProp);
				cachedProp = null;
			});

		}

        const string actionName = "Data Driven Prefab Editor";
        const string menuLocation = "Assets/Create/Prefab Editor/"+ actionName;

        [MenuItem(menuLocation, priority = 81)]
        public static void CreateCustomDataDrivenEditorForPrefab()
        {
            GameObject prefab = Selection.gameObjects[0];
            if (PrefabEditorSettings.Instance.HasDataDrivenEntry(prefab))
            {
                bool ret = EditorUtility.DisplayDialog(actionName, "There is already an entry for this prefab. Do you want to add another entry ? \\n (In doubt, check Project Settings/Custom Prefab Inspectors)", "Yes", "No");
                if (!ret)
                    return;
            }

            
            //1. Extract the folder from the prefab path
            string path = AssetDatabase.GetAssetPath(prefab);
            int indexSlash = path.LastIndexOf("/");
            string folder = path.Substring(0, indexSlash);

            string filename = prefab.name;

            //2. Create data driven object
            DataDrivenEditor ddt= ScriptableObject.CreateInstance<DataDrivenEditor>();
            ddt.name = filename + "_DDEditor";

            AssetDatabase.CreateAsset(ddt, folder + "/" + ddt.name + ".asset");

            //5. Add the Prefab, MonoScript entry in the settings. 
            SerializedObject settingsObj = PrefabEditorSettings.GetOrCreateSerializedSettings();
            SerializedProperty entries = settingsObj.FindProperty(nameof(PrefabEditorSettings.DataDrivenEntries));
            entries.arraySize++;
            SerializedProperty entry = entries.GetArrayElementAtIndex(entries.arraySize - 1);
            entry.FindPropertyRelative(nameof(GameObjectAndDDEditor.prefab)).objectReferenceValue = prefab;
            entry.FindPropertyRelative(nameof(GameObjectAndDDEditor.editor)).objectReferenceValue = ddt;
            //We use the SerializedObject/Properties so the object properly dirtied.
            settingsObj.ApplyModifiedPropertiesWithoutUndo();

            AssetDatabase.OpenAsset(prefab);            

            //Open Asset in default program
            PrefabEditorWindow.ShowWindow();

        }

        [MenuItem(menuLocation, true)]
        public static bool CreateCustomDataDrivenEditorForPrefab_On()
        {
            return Selection.gameObjects.Length == 1;
        }
    }
}