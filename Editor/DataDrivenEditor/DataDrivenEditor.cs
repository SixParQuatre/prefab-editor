using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

namespace SixParQuatre
{
	[Serializable]
	public class DataDrivenEditor: ScriptableObject
	{
		//Set everytime the selection changes
		[NonSerialized]
		public bool canBeCustomized = true;

		public List<DataDrivenEditorLine> props = new List<DataDrivenEditorLine>();

		public void Add(GameObject root, SerializedProperty prop)
		{
			props.Add(new DataDrivenEditorLine(root, prop));
		}
		public void Add(DataDrivenEditorLine prop)
		{
			props.Add(prop);
		}
		
	}
}