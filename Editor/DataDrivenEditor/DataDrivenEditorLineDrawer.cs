using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

namespace SixParQuatre
{
    [CustomPropertyDrawer(typeof(DataDrivenEditorLine), true)]
    public class DataDrivenEditorLineDrawerr : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
           
            SerializedProperty typeProp = property.FindPropertyRelative(nameof(DataDrivenEditorLine.type));
            DataDrivenEditorLine.Type sType = (DataDrivenEditorLine.Type)typeProp.enumValueIndex;
            switch (sType)
            {
                case DataDrivenEditorLine.Type.Property:
                    SerializedProperty ovrd = property.FindPropertyRelative(nameof(DataDrivenEditorLine.OverrideDisplayName));
                    SerializedProperty ovrdVal = property.FindPropertyRelative(nameof(DataDrivenEditorLine.OverrideName));
                    position.width = 20;
                    EditorGUI.PropertyField(position, ovrd, GUIContent.none);
                    position.x += position.width;
                    position.width = 400;

                    if(ovrd.boolValue)
                        EditorGUI.PropertyField(position, ovrdVal, GUIContent.none);
                    else
                        EditorGUI.LabelField(position, property.FindPropertyRelative(nameof(DataDrivenEditorLine.DisplayName)).stringValue);

                    break;
                case DataDrivenEditorLine.Type.Header:
                    EditorGUI.PropertyField(position, property.FindPropertyRelative(nameof(DataDrivenEditorLine.header)), GUIContent.none);
                    break;


            }        
        }
    }
}