﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace SixParQuatre
{
    public static class EditorExt
    {
        public static Object GetTargetSafely(this Editor editor)
        {
            if (editor.targets.Length == 0)
                return null;
            else
                return editor.target;
        }
    }

    /// <summary>
    /// Editor that allow to view all the prefab editors that can be applied to a selected GameObject
    /// contains a drop down list t
    /// </summary>
    public class PrefabEditorsContainer : Editor
    {
        public static PrefabEditorsContainer CreateFor(Object[] targets)
        {
            PrefabEditorsContainer editor = Editor.CreateEditor(targets, typeof(PrefabEditorsContainer)) as PrefabEditorsContainer;
            if (!editor.HasAnyContent())
                return null;
            return editor;
        }

        private void OnEnable()
        {
            Refresh(false);
        }

        public bool HasValidTargets()
        {
            foreach(Object obj in targets)
            {
                if(obj == null)
                    return false;
            }
            return targets.Length > 0;
        }

        public bool HasAnyContent() => inspectors.Count > 0;

        List<PrefabInspector> inspectors = new List<PrefabInspector>();
        int currentIndex = 0;
        List<string> inspectorNames = new List<string>();

        /// <summary>
        /// Create the Editors for the targetObject and repaint the editor if necessary
        /// </summary>
        /// <param name="repaint"></param>
        public void Refresh(bool repaint)
        {

            inspectors.Clear();
            GameObject targetGameObject = this.GetTargetSafely() as GameObject;

            if (targetGameObject == null)
                return;

            inspectors = PrefabEditorSettings.Instance.FindEditorsFor(targets);

            inspectorNames.Clear();
            foreach (PrefabInspector inspector in inspectors)
                inspectorNames.Add(inspector.GetType().Name);
            currentIndex = 0;

            if (repaint)
                Repaint();
        }

        public bool FoundOneEditor() => inspectors.Count > 0;

        public PrefabInspector GetCurrentInspector() => inspectors[currentIndex];

        public bool CurrentIsDatatDriven()
        {
            if (!FoundOneEditor())
                return false;
            return GetCurrentInspector() is DataDrivenPrefabInspector;
        }

        public override void OnInspectorGUI()
        {
            if (!FoundOneEditor())
            {
                EditorGUILayout.HelpBox("No Editor found for the selected object.", MessageType.Info, true);
                return;
            }

            if (inspectors.Count > 1)
            {
                int previousIndex = currentIndex;
                currentIndex = EditorGUILayout.Popup(currentIndex, inspectorNames.ToArray());
            }

            PrefabInspector currentInspector = inspectors[currentIndex];
            if (!currentInspector.TargetsAreValid())
                Refresh(false);
            else
                currentInspector.OnInspectorGUI();
        }

        internal void OnPrefabStageClosing(GameObject prefabContentsRoot)
        {
            inspectors[currentIndex].OnPrefabStageClosed();
        }
    }
}
