If you find this in 2024 and beyond, **Ciro Continisio's [Black Box](https://assetstore.unity.com/packages/tools/utilities/blackbox-improved-prefab-workflow-274430)** does what this package meant to achieve and with an infinitely better UX.<br>
It's worth the money he's asking for if you're on a serious project!<br>
If you want the poor man's, not-maintained version of it, feel free to continue :) <br>

# If you're still interested

The Prefab editor packages is a set of tools that allows and coder to associate an editor to a prefab and variants and their prefab instance.

As of 7th of february 2024, this package is in basically in **Beta 1.2**, please report issue you may have with it or any improvement you can think of.

# Why it's useful

Very often, instances of prefab only modify **a handful of fields/variables/properties to differentiate them from their parent.** 
Sometimes, those values are spread across different components or sub-objects. 
At times, it is also undesirable to have certain variable of instances to be exposed; since changing them make break the convention (e.g. changing the shape of an instance of a collider template named "box").

**Prefab Editors** allow coders and LDs code to make **specific editors to show only the values they want LD to interact with**; making setup and maintenance of variants and instances a lot easier.

The **Prefab Editor Window** allows to access these editors in a specific tab separate from the inspector window.
All prefab editor support displaying errors from the editors in the Editor Window rather than spamming the console.
//insert image of error handling in data driven and code driven

# Data Driven Prefab Editor
## Use-case
Data Driven is the friendlist way to have user gather fields from differents part of a prefab, order them and categorize them.

//insert example of an editor

## How to create one
Select the prefab or a variant you want to create an editor for, then right-click and go into **Create→Prefab Editor→ Data Driven Prefab Editor**
This will:
1. create a ScriptableObject name **{NameOfThePrefab}_DDEditor.prefab** next to the selected prefab. 
2. an entry will be in the Prefab Editor Settings to associate the editor code with the prefab. These can be viewed under **Project Settings/Six-par-Quatre/Prefab Editor**
3. the selected prefab will be opened and the Prefab Editor will be Shown and Opened in Edit mode, so you can start working on it right away 
## How to edit
In the traditionnal inspector, navigate to any properties of the prefab or its sub-object, and right-click on it.
In 'Edit Mode', there will be an option called 'Add to Prefab Editor', click on it to add the field to the . 
Known issue: after adding a new property, the editor window will appear empty, move the mouse over the window to refresh it.

//Add gif of th context menu
# Code Driven Prefab Editor
## Use-case 
Code driven are useful if you want to do more sophisticated things than just displaying of a linear list of properties, such as showing/hiding properties based on values of others.

## How to create  one
Select the prefab or a variant you want to create an editor for, then right-click and go into **Create→Prefab Editor→ C# Prefab Editor**

This will:
1. create a script named **{NameOfThePrefab}Editor.cs** in an **Editor** subfolder under the folder where the prefab is located. 
The spaces in the name will be replaced by underscore. 
If the prefab name starts with a digit, "The" will be inserted at the front.
2. The script will then be opened by your default code editing program; and you can start coding.
3. an entry will be in the Prefab Editor Settings to associate the editor code with the prefab. These can be viewed under **Project Settings/Six-par-Quatre/Prefab Editor** 

## Codebase shortcuts

To lower barrier of entry, th codebase offers several shortcuts to display properties of objects, by hiding _some_ of the complexity of normal Unity Editor coding.
1. Let's say you want to show the variable Type of the Light component 
In **OnEnable_Specific**; you would add this
```csharp
//Register the LightComponent component so changes made in this editor will be properly applied
SerializedObject damageZoneComponent = GetComponents<DamageZone>(Target);

//Register the Type property; use nameof, instead of a "Type" so that somebody refactor this, this will either be renamed automatically or will break at compile time
RegisterProperty(damageZoneComponent, nameof(DamageZone.Type), "Damage Zone Type");  //overriding the default label is useful for field which have too generiic name, it is 

//NB: Registered property will appear in that order in the editor
```

Always use **Target** (capital T), so that the editor works when several objects compatible with that editor are selected.

2. If you wanted to show a variable from a component from a sub-object, whose name is the same across instance:

```csharp
SerializedObject colliderTarget = FindChild(Target, "Collider"); //Find an object with name Colliderr
SerializedObject colliderCpts = GetComponents<SphereCollider>(colliderTarget); //Get and register the collider component
RegisterProperty(colliderCpts, "m_Radius", "Radius Collider"); //here we can't use nameof because it doesn't match the actual property name.
//See Code-tips for how you can figure out a property name		
```

## Code tips

In case:
- a Unity Component doesn't expose the field for a property
- you want to dig deeper into a class that isn't a component
You can use this  function in OnInspectorGUI_Specific()
```csharp
PrintPropertiesName(GetComponents<Light>(Target)); //targetObject can point to a component
```

## For "power"  users
Code Driven Prefab Editor are a form of Unity editors; so you can use the same function you'd use in a normal editor in OnEnable_Specific and OnInspectorGUI_Specific, to display properties, etc...

#Prefab Editor Window
Access it via into: **Windows→General→Prefab Editor**

Upon selecting GameObject in the project or hierarchy windows one of 3 things can happen.

1. If no Prefab Editor is found for any of the prefab parents, the editor will show this.
2. If one editor is found, the window will display it.
3. If more than one is found(this may happen when an editor), the window will display the one from the closest Prefab, and will feature a dropdown list to select other availables. 

    This may be useful to have more and more specialized editor for more and more specialized variants.
	
