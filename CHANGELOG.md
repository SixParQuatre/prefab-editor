Description of package changes in reverse chronological order. It is good practice to use a standard format, like [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
# Changelog

## [0.1.0] - 2024-2-2

### Changed
Moved the context menu to create Prefab Editors under Create->Prefab Editor

### Added
DataDrivenPrefabEditor; which allows to build a Prefab Editor without coding  in C#, by selecting properties in the Inspector and arranging them in the PrefabEditor window. See ReadMe for details

### Known Issues
- After adding a new property, the PrefabEditor is blank until the user mov the  mouse cursor over it.
- When leaving Editor Customization, a GUI Layout error is thrown.
- Changes to DataDrivenEditor are not reliable save to disk after clicking Done; user should save the assets themselves.